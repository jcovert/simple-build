
var debug = require('debug')('metalsmith-collections');
var extend = require('extend');
var Matcher = require('minimatch').Minimatch;
var unique = require('uniq');
var fs = require('fs');
var loadMetadata = require('read-metadata').sync;

/**
 * Expose `plugin`.
 */

module.exports = plugin;

/**
 * Metalsmith plugin that adds `collections` of files to the global
 * metadata as a sorted array.
 *
 * @param {Object} collections (optional)
 * @return {Function}
 */

function plugin(opts){
	//console.log(opts);
  opts = normalize(opts);
  var keys = Object.keys(opts);
  var match = matcher(opts);

  return function(files, metalsmith, done){
    var metadata = metalsmith.metadata();
    /**
     * Find the files in each collection.
     */
    // metadata[key] = metadata[key] || [];
     //   metadata[key].push(data);

    function createEmptyCollectionPage(collection, settings, key, lang){
	    for(var i = 0; i < collection.length; i++){
		         var page = {
		          layout: settings.layout,
		          contents: '',
		          tag: collection[i].ID,
		          title: collection[i].Title,
		          locale: collection[i].lang || lang,
		          pagination: {
		            tag: key,
		            files: []
		          }
		        };
		        // console.log("create page " + collection[i].URL);
		        // Add new page to files object.
		        if(collection[i].URL)
			        files[collection[i].URL + "/index.html"] = page;

		        if(collection[i].children){
			        createEmptyCollectionPage(collection[i].children, settings, key, collection[i].lang)
		        }
	        }
    }

    /**
     * Ensure that a default empty collection exists.
     */

    keys.forEach(function(key) {
	    var settings = opts[key];
	    var path = settings.json;
	    var data = fs.readFileSync(path, 'utf-8');
	    var res = JSON.parse(data);
		//console.log(metadata[key]);
		if(metadata[key]){
			metadata[key] = metadata[key].concat(res);
		}else{
			metadata[key] = res;
		}

        //metadata.collections[key] = res;
        if(settings.layout){

	       	   createEmptyCollectionPage(res, settings, key)    ;

        }

    });




    done();
  };
}

/**
 * Normalize an `options` dictionary.
 *
 * @param {Object} options
 */

function normalize(options){
  options = options || {};

  for (var key in options) {
    var val = options[key];
    if ('string' == typeof val) options[key] = { pattern: val };
  }

  return options;
}

/**
 * Generate a matching function for a given set of `collections`.
 *
 * @param {Object} collections
 * @return {Function}
 */

function matcher(cols){
  var keys = Object.keys(cols);
  var matchers = {};

  keys.forEach(function(key){
    var opts = cols[key];
    if (!opts.pattern) return;
    matchers[key] = new Matcher(opts.pattern);
  });

  return function(file, data){
    var matches = [];
	if (data.collection) {
      var collection = data.collection;
      if (!Array.isArray(collection)) collection = [collection];
      collection.forEach(function(key){
        matches.push(key);

        if (key && keys.indexOf(key) < 0)
          debug('adding new collection through metadata: %s', key);
      });
    }

    for (var key in matchers){
      var m = matchers[key];
      if (m && m.match(file)) matches.push(key);
    }

    data.collection = unique(matches);
    return data.collection;
  };
}
