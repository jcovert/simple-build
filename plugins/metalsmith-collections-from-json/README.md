
# metalsmith-collections-from-json

A Metalsmith plugin to create dynamic collections from json files.

## Javascript Usage

  Pass `options` to the markdown plugin and pass it to Metalsmith with the `use` method:

```js
var collectionsFromJson = require('./plugins/metalsmith-collections-from-json');

.use(collectionsFromJson({
    categories: {
      json: RESOURCES_PATH + "/json/blog_categories.json",
      sortBy: "publish",
      reverse: true,
      layout:'category-index-layout.html'
    }
    
  }))
```

## Options


