'use strict';

var
  metalsmith = require('metalsmith'),
  markdown   = require('metalsmith-markdown'),
  layouts    = require('metalsmith-layouts'),
  Handlebars = require('handlebars'),
  cheerio    = require('cheerio'),
  aliases    = require('metalsmith-aliases'),
  permalinks = require('metalsmith-permalinks'),
  assets     = require('metalsmith-assets'),
  collections = require('metalsmith-collections'),
  paginate = require('./plugins/metalsmith-advanced-pagination'),
  collectionsFromJson = require('./plugins/metalsmith-collections-from-json'),
  ignore = require('metalsmith-ignore'),

  templateConfig = {
    engine:     'handlebars',
    directory:  'src/main/assets/layouts/',
    partials:   'src/main/assets/partials/'
  };

  Handlebars.registerHelper('getPartialName', function(name) { return name });

  Handlebars.registerHelper('debug', function(object) {
    console.log(object);
  });

  Handlebars.registerHelper('paginifyPath', function(path) {
    return "/" + path.substring(0, path.indexOf('index.html'));
  });

  Handlebars.registerHelper('loadContent', function(context, contentHtml) {
    var contentModules = [];
    var sections = contentHtml.split('<!---MODULE=');
    for (var i = 1; i < sections.length; i++) {
      var parts = sections[i].split('--->');
      var nameAnchor = parts[0].split(",ANCHOR=");

      var anchorName = '';
      var moduleName = nameAnchor[0];
      if (nameAnchor.length > 1) {
        anchorName = nameAnchor[1];
      }
      var moduleContent = cheerio.load(parts[1]);

      var $ = moduleContent;
          $('p a:only-child, img:only-child').each(function() {
            /**
             * We selected only p tags with a single a or img
             * but we could get those with text nodes too. So
             * we will filter for nodeType 3 which is text node.
             */
              var count = $(this).parent().contents().filter(
                function() {
                return this.nodeType === 3;
              }).length;

            /**
             * If we get zero back, then there is not text and
             * this was in fact a lone img or a tag.
             */
            if (count == 0) {
                var $p = $(this).parent();
                      $(this).insertAfter($(this).parent());
                      $p.remove()
            }
          });

      contentModules[i - 1] = { name: moduleName, content: moduleContent, anchor: anchorName };
    }
    context['contentModules'] = contentModules;
  });

  Handlebars.registerHelper('isDemoVideo', function (isDemo, options) {
    if (isDemo === "1") {
      return options.fn(this);
  	}
  	return options.inverse(this);
  });


  Handlebars.registerHelper('getContent', function(context, tag) {

    // console.log("Getting content: " + context.name + " - " + tag);

    if (!context['tagRecord']) { context['tagRecord'] = {}; }

    var element = tag;
    var parts = tag.split(".");
    if (parts.length > 1) {
      element = parts[0];
    }

    var tagKey = context.name + "_" + element;
    var tagIndex = context['tagRecord'][tagKey];
    tagIndex = tagIndex != undefined ? tagIndex + 1 : 0;
    context['tagRecord'][tagKey] = tagIndex;

    var $ = context.content;

    var value = '';
    if (parts.length > 1) {
      var wrappedTag = wrapElement($(parts[0]).eq(tagIndex));
      value = $(wrappedTag).attr(parts[1]);
    }
    else {
      value = wrapElement($(tag).eq(tagIndex));
    }
    //console.log(tag + ": " + value);
    return value;
  });

  function wrapElement(tag) {
    return tag.clone().wrap('<div/>').parent().html()
  }

  Handlebars.logger.level = 0;
  Handlebars.registerHelper('categoryMatch', function(val, path, options) {

    path = path.substring(0, path.lastIndexOf('/'));
    path = path.split('/').reverse()[0]

    if (path === val || path === 'blog') {
//      console.log("val: " + val);
//      console.log("path: " + path);
  		return options.fn(this);
  	}
  	return options.inverse(this);
  });

var ms = metalsmith(__dirname) // the working directory
    .clean(true)            // clean the build directory
    .source('src/main/content/en')    // the page source directory
    .destination('target/www')  // the destination directory
    // .ignore([""])

    // .use(collectionsFromJson({
    //     manualCollection: {
    //       json: "src/main/assets/json/manual_collections.json",
    //       sortBy: "name",
    //       reverse: true,
    //       layout:'collection-layout.html'
    //     }
    // }))

    .ignore([
      "**/assets/layouts/**"
    ])

    .use(collections({
      beagle: {
        pattern: "**/collections/*/*.md",
        sortBy: "title",
        reverse: true
      },
      blog: {
        pattern: "**/blog/*/*.md",
        sortBy: "name",
        reverse: false
      },
      blog_category: {
        pattern: "**/blog/category/**/*.md",
        sortBy: "name",
        reverse: false
      },
    }))

    .use(paginate({
  		beagle: {
  			perPage: 2,
  			layout: 'collections-index-page.html',
  			first: 'collections/index.html',
  			path: 'collections/:num/index.html',
        noPageOne: true,
  			locale: 'en'
  		}
    }))


    .use(markdown())        // convert markdown to HTML
    .use(layouts(templateConfig))

    .use(permalinks())
    .use(aliases({
      redirect: "true"
    }))

    .use(assets({
      source: "src/main/assets",
      destination: "./assets"
    }))

    .build(function(err) {  // build the site
      if (err) throw err;   // and throw errors
    });

function isEmpty( obj ) {
  for ( var name in obj ) {
    return false;
  }
  return true;
}
