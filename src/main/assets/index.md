---
title: Module Tester
layout: dynamic-layout.html
---

<!---MODULE=mod-1-2-sub-nav--->

[Three](#three)

[Cards](#cards)



<!---MODULE=mod-3-1a-type-large-graphic--->

![alt text](/assets/images/logo_mapr_red.png)

## Introducing MapR Converged Data Platform for Docker Containers

The new generation of converged applications is as much about deploying operational, user-facing applications as performing analytics and machine learning. Containers are the future of application deployment...

[More Information](/products/persistent-application-client-container/)

<!---MODULE=mod-3-1a-type-large-graphic--->

![alt text](/assets/images/logo_mapr_red.png)

## Introducing MapR Converged Data Platform for Docker Containers

The new generation of converged applications is as much about deploying operational, user-facing applications as performing analytics and machine learning. Containers are the future of application deployment...

[More Information](/products/persistent-application-client-container/)



<!---MODULE=mod-3-1a-type-large-graphic,ANCHOR=three--->

![alt text](/assets/images/logo_mapr_red.png)

## THREE MapR Converged Data Platform for Docker Containers

The new generation of converged applications is as much about deploying operational, user-facing applications as performing analytics and machine learning. Containers are the future of application deployment...

[More Information](/products/persistent-application-client-container/)



<!---MODULE=mod-3-1a-type-large-graphic--->

![alt text](/assets/images/logo_mapr_red.png)

## Introducing MapR Converged Data Platform for Docker Containers

The new generation of converged applications is as much about deploying operational, user-facing applications as performing analytics and machine learning. Containers are the future of application deployment...

[More Information](/products/persistent-application-client-container/)




<!---MODULE=mod-4-1b-3up-image-cards,ANCHOR=cards--->

[![Security Log Analytics](/assets/images/logo_mapr_red.png)](https://www.youtube.com/embed/ElszuK0VeXU?autoplay=1&rel=0)

### CARDS BTW

[Watch the Video](https://www.youtube.com/embed/ElszuK0VeXU?autoplay=1&rel=0)


[![Placeholder](/assets/images/logo_mapr_red.png)](/ebooks/big-data-all-stars/)

### Big Data All-Stars

[Read the ebook](/ebooks/big-data-all-stars/)


[![Saum Mathur, HP Global CIO explains how HP uses MapR](/assets/images/logo_mapr_red.png)](https://www.youtube.com/embed/9sUHO7tLmHI?autoplay=1&rel=0)

### Saum Mathur, HP Global CIO explains how HP uses MapR

[Watch the Video](https://www.youtube.com/embed/9sUHO7tLmHI?autoplay=1&rel=0)


<!---MODULE=mod-3-1a-type-large-graphic-blog-mod--->

## Blog

The MapR blog provides how-to advice, insights, best practices, and useful resources to help your executives, enterprise architects, and developers more effectively leverage data to grow your business.

[Go to the blog](/blog/)
